use std::{fs, process};
use std::path::Path;

use arg_dot_h::*;

use walkdir::WalkDir;

fn help(argv0: &str) {
    println!(
"{argv0} [-afipv] [-R [-H | -L | -P]] src target
{argv0} [-afipv] [-R [-H | -L | -P]] src ... directory

Copy files

Copyright (c) 2021 BubbyRoosh

Arguments:
-a  Archive mode. Same as -RP.
-f  Existing destinations will be replaced without prompting. Overrides
    previous -i.
-H  If -R, symbolic arguments are followed, but tree traversal links are not.
-i  Write a prompt to standard error before copying a file. Overrides
    previous -f.
-L  If -R, all symbolic links are followed.
-P  If -R, no symbolic links are followed.
-p  Compatability. Copy will preserve file information regardless.
-R  Copy src and its entire subtree if it's a direcotry.
-r  Equivalent to -R
-v  Display src and dest after each copy.
", argv0 = argv0);
    process::exit(1);
}

fn cp(path: &Path, dest: &Path, fv_verbose: bool) {
    let name = path.to_string_lossy();
    if path.is_dir() {
        if let Err(e) = fs::create_dir_all(dest) {
            eprintln!("Couldn't copy {}: {}", name, e);
        } else if fv_verbose {
            println!("{}", name);
        }
    } else {
        let newdest = if dest.is_dir() {
            dest.join(path.file_name().unwrap())
        } else {
            dest.to_path_buf()
        };
        if let Err(e) = fs::copy(path, newdest) {
            eprintln!("Couldn't copy {}: {}", name, e);
        } else if fv_verbose {
            println!("{}", name);
        }
    }
}

fn try_copy(path: &Path, dest: &Path, fv_verbose: bool, fsh_dont_follow_tree: bool, fsl_follow_all: bool, fsp_follow_none: bool) {
    if path.is_dir() {
        let should_follow = !(fsh_dont_follow_tree && fsp_follow_none) && fsl_follow_all;
        WalkDir::new(path)
            .follow_links(should_follow)
            .into_iter()
            .filter_map(|e| e.ok())
            .for_each(|entry| {
                if entry.path().is_dir() {
                    cp(entry.path(), dest.join(entry.path()).as_path(), fv_verbose);
                } else {
                    cp(entry.path(), dest, fv_verbose);
                }
            });
    } else {
        cp(path, dest, fv_verbose);
    }
}

fn main() {
    let mut argv0 = String::new();
    let mut ff_dont_prompt_replace = false;
    let mut fsh_dont_follow_tree = false;
    let mut fi_prompt_all = false;
    let mut fsl_follow_all = false;
    let mut fsp_follow_none = false;
    let mut fsr_recurse = false;
    let mut fv_verbose = false;
    let mut filenames = argbegin! {
        &mut argv0,
        'a' => {fsr_recurse = true; fsp_follow_none = true},
        'f' => {ff_dont_prompt_replace = true; fi_prompt_all = false},
        'H' => if fsr_recurse {fsh_dont_follow_tree = true; fsp_follow_none = false; fsl_follow_all = false},
        'i' => {fi_prompt_all = true; ff_dont_prompt_replace = false},
        'L' => if fsr_recurse {fsl_follow_all = true; fsp_follow_none = false; fsh_dont_follow_tree = false},
        'P' => if fsr_recurse {fsp_follow_none = true; fsl_follow_all = false; fsh_dont_follow_tree = false},
        'p' => {},
        'R' => fsr_recurse = true,
        'r' => fsr_recurse = true,
        'v' => fv_verbose = true,
        _ => help(&argv0)
    };

    if filenames.len() < 2 {
        help(&argv0);
    }

    let deststring = filenames.remove(filenames.len() - 1);
    let dest = Path::new(&deststring);

    if dest.exists() && !dest.is_dir() && filenames.len() > 1 {
        help(&argv0);
    }

    for filename in filenames {
        let dest = Path::new(&deststring);
        let path = Path::new(&filename);

        if common::fs::can_alter(path, fsr_recurse, fsr_recurse) &&
            common::fs::should_alter(
                &format!("Copy {}", filename),
                &dest,
                ff_dont_prompt_replace,
                fi_prompt_all) {

            try_copy(path,
                dest,
                fv_verbose,
                fsh_dont_follow_tree,
                fsl_follow_all,
                fsp_follow_none);
        }
    }
}
