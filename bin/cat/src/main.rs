use std::{error, fs, path, process};
use std::io::*;
use std::result::Result;
use std::os::unix::io::FromRawFd;

use arg_dot_h::*;

fn help(argv0: &str) {
    println!(
"{} [-benstuv] file ...

Concatenate and print files

Copyright (c) 2021 BubbyRoosh

Arguments:
-b  Number the lines excluding blank lines.
-e  Print a dollar sign ('$') at the end of each line. Implies -v.
-n  Number the output lines, starting at 1.
-s  Squeeze multiple adjacent empty lines, causing the output to be single
    spaced.
-t  Print tab characters as '^I'. Implies -v.
-u  The output is guaranteed to be unbuffered.
-v  Displays non-printing chars so they are visible.
", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut fb_numbernonblank = false;
    let mut fe_eol = false;
    let mut fn_numberall = false;
    let mut fs_squeeze = false;
    let mut ft_tabs = false;
    let mut fu_unbuffered = false;
    let mut fv_nonprinting = false; // TODO: Unimplemented: Idk how to test this cause idk what these "nonprinting" chars are really...
    let filenames = argbegin! {
        &mut argv0,
        'b' => fb_numbernonblank = true,
        'e' => {fe_eol = true; fv_nonprinting = true;},
        'n' => fn_numberall = true,
        's' => fs_squeeze = true,
        't' => {ft_tabs = true; fv_nonprinting = true;},
        'u' => fu_unbuffered = true,
        'v' => fv_nonprinting = true,
        _ => help(&argv0)
    };

    let parse_line = |idx: usize, line: &mut String| -> bool {
        let mut out = String::new();
        let mut should_inc = true;
        if fn_numberall || fb_numbernonblank {
            let idx = idx.to_string();
            let idxlen = idx.len();
            // 6 chars because that's what I counted from OpenBSD cat
            for _ in 0..6-idxlen {out.push(' ')}
            if !(line.trim().is_empty() && fb_numbernonblank) {
                out.push_str(&format!("{}  ", idx));
            } else {
                should_inc = false;
                out.push_str("   ");
            }
        }
        line.insert_str(0, &out);
        if fe_eol {line.push('$');}
        if ft_tabs {*line = line.replace("\t", "^I");}
        should_inc
    };

    let parse_file = |filename| -> Result<String, Box<dyn error::Error>> {
        let mut out = String::new();
        let path = path::Path::new(filename);
        if path.is_dir() {
            eprintln!("{} is a directory", filename);
            return Ok(String::new());
        }
        let contents = common::fs::read_to_string_lossy(filename)?;

        let mut last_line_empty = false;
        let mut idx: usize = 0;
        for line in contents.lines() {
            let mut line = line.to_owned();
            let is_empty = line.trim().is_empty();
            if is_empty && fs_squeeze && last_line_empty {
                continue;
            }
            last_line_empty = is_empty;
            let should_inc = parse_line(idx + 1, &mut line);
            out.push_str(&line);
            out.push('\n');

            if should_inc {idx += 1;}
        }

        Ok(out.trim_end().to_string())
    };

    let stdout = if fu_unbuffered {
        unsafe {
            Some(fs::File::from_raw_fd(1))
        }
    } else {
        None
    };

    let mut use_stdin = false;

    if filenames.is_empty() {use_stdin = true;}
    filenames.iter().for_each(|filename| {
        if filename == "-" {
            use_stdin = true;
        } else {
            let mut out = match parse_file(filename) {
                Ok(o) => o,
                Err(e) => {
                    eprintln!("Error parsing file {}: {}", filename, e);
                    process::exit(1);
                }
            };
            if fu_unbuffered {
                out.push('\n');
                stdout.as_ref().unwrap().write_all(out.as_bytes()).unwrap();
            } else {
                println!("{}", out);
            }
        }
    });

    if use_stdin {
        let mut idx = 0;
        let mut last_line_empty = false;
        let stdin = stdin();
        for line in stdin.lock().lines().filter_map(|l| l.ok()) {
            let mut line = line.to_owned();
            let is_empty = line.trim().is_empty();
            if is_empty && fs_squeeze && last_line_empty {
                continue;
            }
            last_line_empty = is_empty;
            let should_inc = parse_line(idx + 1, &mut line);

            if should_inc {idx += 1;}
            if fu_unbuffered {
                line.push('\n');
                stdout.as_ref().unwrap().write_all(line.as_bytes()).unwrap();
            } else {
                println!("{}", line);
            }
        }
    }
}
