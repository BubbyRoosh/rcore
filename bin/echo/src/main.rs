use std::env;
fn main() {
    let mut args = env::args().collect::<Vec<_>>();
    args.remove(0);
    let mut out = String::new();
    let mut nonewline = false;
    for arg in args {
        if arg == "-n" {
            nonewline = true;
        } else {
            // Rust only supports \x1b escape chars so \033 needs to be replaced.
            out.push_str(&format!("{} ", arg.replace("\\033", "\x1b")));
        }
    }

    if nonewline {
        print!("{}", out.trim());
    } else {
        println!("{}", out.trim());
    }
}
