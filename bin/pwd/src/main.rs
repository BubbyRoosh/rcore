use std::{env, fs, process};

use arg_dot_h::*;

fn help(argv0: &str) {
    println!(
"{} [-LP]

Return working directory name

Copyright (c) 2021 BubbyRoosh

Arguments:
-L  Print PWD unless it uses \".\" or \"..\". Overrides previous -P
-P  Print absolute physical path, resolving symlinks. Overrides previous -L.
", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut fsl_absolute_or_fsp = false;
    let mut fsp_physical_path = false;
    argbegin! {
        &mut argv0,
        'L' => {fsl_absolute_or_fsp = true; fsp_physical_path = false;},
        'P' => {fsp_physical_path = true; fsl_absolute_or_fsp = false;},
        _ => help(&argv0)
    };

    let pwd_env = env::var("PWD").unwrap();
    if fsl_absolute_or_fsp {
        let split = pwd_env.split('/').collect::<Vec<_>>();
        if split.contains(&"..") || split.contains(&".") {
            fsp_physical_path = true;
        } else {
            println!("{}", pwd_env);
            return;
        }
    }

    if fsp_physical_path {
        println!("{}", fs::canonicalize(&pwd_env).unwrap().to_string_lossy());
    } else {
        match nix::unistd::getcwd() {
            Ok(pbuf) => println!("{}", pbuf.to_string_lossy()),
            Err(e) => eprintln!("Error getting cwd: {}", e),
        }
    }
}
