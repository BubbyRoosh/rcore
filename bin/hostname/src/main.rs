use std::process;

use arg_dot_h::*;

use nix::unistd;

fn help(argv0: &str) {
    println!(
"{} [-s] hostname

Set or print name of current host system

Copyright (c) 2021 BubbyRoosh

Arguments:
-s  Trims off any domain information from the printed name.
", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut fs_trim = false;
    let args = argbegin! {
        &mut argv0,
        's' => fs_trim = true,
        _ => help(&argv0)
    };

    match args.get(0) {
        Some(name) => {
            if let Err(e) = unistd::sethostname(name) {
                eprintln!("Error setting hostname: {}", e);
            }
        },
        None => {
            let mut buf = [0u8; 64];
            let hostname_cstr = unistd::gethostname(&mut buf)
                .expect("Failed to get hostname");

            let hostname = hostname_cstr.to_string_lossy();

            if fs_trim {
                println!("{}", hostname.split('.').next().unwrap());
            } else {
                println!("{}", hostname);
            }
        }
    }
}
