use std::{fs, process};
use std::os::unix::fs::PermissionsExt;

use arg_dot_h::*;

fn help(argv0: &str) {
    println!(
"{} [-p] [-m mode] directory ...

Make directories

Copyright (c) 2021 BubbyRoosh

Arguments:
-m <mode>   Set the file permission bits of the newly created directory
            to <mode>.
-p          Create directories recursively.
", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut om_mode: u32 = 0o777;
    let mut fp_recurse = false;
    let dirs = argbegin! {
        &mut argv0,
        'm' => {
            let mode = eargf!(help(&argv0)).parse::<u32>();
            om_mode = mode.unwrap_or_else(|e| {
                eprintln!("Error getting mode: {}. Defaulting to 0777", e);
                0o777
            });
        },
        'p' => fp_recurse = true,
        _ => help(&argv0)
    };

    dirs.iter().for_each(|dir| {
        if fp_recurse {
            if let Err(e) = fs::create_dir_all(dir) {
                eprintln!("Error creating {}: {}", dir, e);
            }
        } else if let Err(e) = fs::create_dir(dir) {
            eprintln!("Error creating {}: {}", dir, e);
        }

        if let Ok(file) = fs::OpenOptions::new().write(true).open(dir) {
            let mut perms = file.metadata().unwrap().permissions();
            perms.set_mode(om_mode);
            fs::set_permissions(dir, perms).unwrap();
        }
    });
}
