use std::{fs, path, process};

use arg_dot_h::*;

fn help(argv0: &str) {
    println!(
"{argv0} [-fiv] src target
{argv0} [-fiv] src ... directory

Move files

Copyright (c) 2021 BubbyRoosh

Arguments:
-f  Don't prompt if overwriting destination path. Overrides previous -i.
-i  Write a prompt to standard error before moving a file. Overrides
    previous -f.
-v  Display src and dest after each move.
", argv0 = argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut ff_dont_prompt_overwrite = false;
    let mut fi_prompt_all = false;
    let mut fv_verbose = false;
    let mut filenames = argbegin! {
        &mut argv0,
        'f' => {ff_dont_prompt_overwrite = true; fi_prompt_all = false;},
        'i' => {fi_prompt_all = true; ff_dont_prompt_overwrite = false;},
        'v' => fv_verbose = true,
        _ => help(&argv0)
    };

    if filenames.len() < 2 {help(&argv0);}

    let deststring = filenames.pop().unwrap();
    let dest = path::Path::new(&deststring);

    let moving_to_dir = dest.exists() && dest.is_dir();

    filenames.iter().for_each(|src| {
        let path = path::Path::new(src);
        let dest = if moving_to_dir {
            path::Path::new(&deststring).join(path.file_name().unwrap())
        } else {
            path::PathBuf::from(&deststring)
        };

        if common::fs::should_alter(
            &format!("Move {} to {}", src, dest.to_string_lossy()),
            &dest,
            ff_dont_prompt_overwrite,
            fi_prompt_all) {
            if let Err(e) = fs::rename(path, &dest) {
                eprintln!("Error moving {} to {}: {}", src, dest.to_string_lossy(), e);
            } else if fv_verbose {
                println!("{} -> {}", src, dest.to_string_lossy());
            }
        }
    });
}
