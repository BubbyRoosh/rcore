// first program :D

// std imports first
use std::{cmp, fs, process};
use std::convert::TryInto;
use std::io::*;
use std::path::Path;

// Crates afterwards. Each diff crate is separated by an empty line
use arg_dot_h::*;

use rand::Rng;
use rand::rngs::ThreadRng;

use walkdir::WalkDir;

fn help(argv0: &str) {
    println!(
"{} [-dfiPRrv] file ...

Remove directory entries

Copyright (c) 2021 BubbyRoosh

Arguments:
-d  Attempt to remove directories as well as other types of files.
-f  Only for compatability. No prompts will be given by default.
-i  Request confirmation before attempting to remove each file.
-P  Attempt to overwrite writable files before deleting them with a random
    pattern.
-R  Attemt to remove the file hierarchy, implying -d.
-r  Equivalent to -R.
-v  Display each file name after it's removed.
", argv0);
    process::exit(1);
}

fn rm(path: &Path, fsp_overwrite: bool, fv_verbose: bool, rng: &mut ThreadRng) {
    let name = path.to_string_lossy();
    if path.is_dir() {
        if let Err(e) = fs::remove_dir(path) {
            eprintln!("Couldn't remove {}: {}", name, e);
        } else if fv_verbose {
            println!("{}", name);
        }
    } else {
        if fsp_overwrite {
            // https://users.rust-lang.org/t/idiomatic-way-to-generate-a-file-of-a-given-size/30407/2
            if let Ok(file) = fs::OpenOptions::new().write(true).open(path) {
                let mut remaining: usize = file.metadata().unwrap().len().try_into().unwrap();
                let mut writer = BufWriter::new(&file);
                let mut buffer = [0; 1024];
                while remaining > 0 {
                    let to_write = cmp::min(remaining, buffer.len());
                    let buffer = &mut buffer[..to_write];
                    rng.fill(buffer);
                    writer.write_all(buffer).unwrap();
                    remaining -= to_write;
                }
            }
        }

        if let Err(e) = fs::remove_file(path) {
            eprintln!("Couldn't remove {}: {}", name, e);
        } else if fv_verbose {
            println!("{}", name);
        }
    }
}

fn try_remove(path: &Path, fsp_overwrite: bool, fv_verbose: bool, rng: &mut ThreadRng) {
    if path.is_dir() {
        let mut reversed = Vec::new();
        for entry in WalkDir::new(path).into_iter().filter_map(|e| e.ok()) {
            reversed.push(entry);
        }
        // .rev() not in into_iter() for WalkDir
        reversed.iter().rev().for_each(|entry| rm(entry.path(), fsp_overwrite, fv_verbose, rng));
    } else {
        rm(path, fsp_overwrite, fv_verbose, rng);
    }
}

fn main() {
    // To be passed to the argument parser because the programs can (but shouldn't) be renamed.
    let mut argv0 = String::new();
    // Flag arguments should be prepended with 'f' followed by the char they are flagged by, then
    // what the argument does (in this case, a flag for 'd' that sets removedirs)
    let mut fd_removedirs = false;
    let mut fi_askall = false;
    // Because rust thinks a capital letter in a name = camelCase, if the char is capital, use 's'
    // before the character
    let mut fsp_overwrite = false;
    let mut fsr_recurse = false;
    let mut fv_verbose = false;
    let filenames = argbegin! {
        &mut argv0,
        'd' => fd_removedirs = true,
        'f' => {}, // Compatability
        'i' => fi_askall = true,
        'P' => fsp_overwrite = true,
        'R' => {fsr_recurse = true; fd_removedirs = true;},
        'r' => {fsr_recurse = true; fd_removedirs = true;},
        'v' => fv_verbose = true,
        _ => help(&argv0)
    };

    // Can't remove files if there are no files to remove
    if filenames.is_empty() {
        eprintln!("No files provided");
        help(&argv0);
    }

    for filename in filenames {
        let path = Path::new(&filename);
        let name = path.to_string_lossy();
        let mut rng = rand::thread_rng();
        if common::fs::can_alter(path, fd_removedirs, fsr_recurse) {
            let should_remove;
            if fi_askall {
                should_remove = common::io::yes_no(&format!("Remove {}?", name), false);
            } else {
                should_remove = true;
            }

            if should_remove {
                try_remove(path, fsp_overwrite, fv_verbose, &mut rng);
            }
        } else {
            eprintln!("Can't remove {}", name);
        }
    }
}
