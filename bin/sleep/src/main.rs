use std::{env, process, thread, time};

fn main() {
    let mut args = env::args();
    let argv0 = args.next().unwrap();

    if args.len() < 1 {
        eprintln!("{} <seconds>", argv0);
        process::exit(1);
    }

    let seconds = match args.next().unwrap().parse::<u64>() {
        Ok(s) => s,
        Err(e) => {
            eprintln!("<seconds> is invalid: {}", e);
            process::exit(1);
        },
    };

    thread::sleep(time::Duration::from_secs(seconds));
}
