use std::{fs, io, path, process};
use std::os::unix::fs::{MetadataExt, PermissionsExt};
use std::time::{UNIX_EPOCH, Duration};

use arg_dot_h::*;

use chrono::Local;
use chrono::prelude::DateTime;

#[derive(Clone, Copy, Debug)]
struct Args {
    fc_sort_last_changed: bool,
    fsc_columns: bool,
    ft_sort_modified: bool,
    fu_sort_access_time: bool,
    fg_no_owner: bool,
    fh_units: bool,
    fi_inodes: bool,
    fk_kilos: bool,
    fl_long: bool,
    fm_stream: bool,
    fn_uidgid_numeric: bool,
    fr_reverse: bool,
    fs_display_blocks: bool,
    fsa_list_all: bool,
    fsf_exe_sym: bool,
    fsh_follow_cmd: bool,
    fsl_follow_info: bool,
    fsp_display_path: bool,
    fsr_recurse: bool,
    fst_display_full_time: bool,
}

impl Args {
    fn fc_sort_last_changed(&mut self, d: bool)  -> &mut Self {self.fc_sort_last_changed  = d; self}
    fn fsc_columns(&mut self, d: bool)           -> &mut Self {self.fsc_columns           = d; self}
    fn ft_sort_modified(&mut self, d: bool)      -> &mut Self {self.ft_sort_modified      = d; self}
    fn fu_sort_access_time(&mut self, d: bool)   -> &mut Self {self.fu_sort_access_time   = d; self}
    fn fg_no_owner(&mut self, d: bool)           -> &mut Self {self.fg_no_owner           = d; self}
    fn fh_units(&mut self, d: bool)              -> &mut Self {self.fh_units              = d; self}
    fn fi_inodes(&mut self, d: bool)             -> &mut Self {self.fi_inodes             = d; self}
    fn fk_kilos(&mut self, d: bool)              -> &mut Self {self.fk_kilos              = d; self}
    fn fl_long(&mut self, d: bool)               -> &mut Self {self.fl_long               = d; self}
    fn fm_stream(&mut self, d: bool)             -> &mut Self {self.fm_stream             = d; self}
    fn fn_uidgid_numeric(&mut self, d: bool)     -> &mut Self {self.fn_uidgid_numeric     = d; self}
    fn fr_reverse(&mut self, d: bool)            -> &mut Self {self.fr_reverse            = d; self}
    fn fs_display_blocks(&mut self, d: bool)     -> &mut Self {self.fs_display_blocks     = d; self}
    fn fsa_list_all(&mut self, d: bool)          -> &mut Self {self.fsa_list_all          = d; self}
    fn fsf_exe_sym(&mut self, d: bool)           -> &mut Self {self.fsf_exe_sym           = d; self}
    fn fsh_follow_cmd(&mut self, d: bool)        -> &mut Self {self.fsh_follow_cmd        = d; self}
    fn fsl_follow_info(&mut self, d: bool)       -> &mut Self {self.fsl_follow_info       = d; self}
    fn fsp_display_path(&mut self, d: bool)      -> &mut Self {self.fsp_display_path      = d; self}
    fn fsr_recurse(&mut self, d: bool)           -> &mut Self {self.fsr_recurse           = d; self}
    fn fst_display_full_time(&mut self, d: bool) -> &mut Self {self.fst_display_full_time = d; self}

    fn new() -> Self {
        Self {
            fc_sort_last_changed: false,
            fsc_columns: false,
            ft_sort_modified: false,
            fu_sort_access_time: false,
            fg_no_owner: false,
            fh_units: false,
            fi_inodes: false,
            fk_kilos: false,
            fl_long: false,
            fm_stream: false,
            fn_uidgid_numeric: false,
            fr_reverse: false,
            fs_display_blocks: false,
            fsa_list_all: false,
            fsf_exe_sym: false,
            fsh_follow_cmd: false,
            fsl_follow_info: false,
            fsp_display_path: false,
            fsr_recurse: false,
            fst_display_full_time: false,
        }
    }
}

fn help(argv0: &str) {
    println!(
"{} [-1AaCcdFfgHhikLlmnopqRrSsTtux] file ...

List directory contents

Copyright (c) 2021 BubbyRoosh

Arguments:
-1  Force output to be one entry per line.
-A  List all entries except for \".\" and \"..\".
-a  Compatability. Equivalent to -A
-C  Force multi-column output; this is the default when the output is to a
    terminal and long formatting is not used (outputs right-left instead of
    the more common top-down).
-c  Use time file's status was last changed instead of last modification time
    for sorting
-d  Compatability.
-F  Display \"/\" immediately after each pathname that is a directory, \"*\"
    after each that is executable, and \"@\" after each symbolic link.
-f  Compatability. No sort args = unsorted.
-g  List like -l without printing the owner.
-H  Follow symbolic links specified on the command line.
-h  Use unit suffixes with -l: Byte/Kilobyte/Megabyte/Gigabyte/...
-i  Print each file's inode number.
-k  Causes sizes to be in kilobytes.
-L  List information about a symbolic link's target instead of the link itself.
-l  List in long format.
-m  Stream output format; list comma-separated files across the page.
-n  List in long format but retain UIDs and GIDs in numeric format.
-o  Compatability. -
-P  Display \"/\" after each path that is a directory.
-q  Compatability. Files will be printed as utf-8 lossy strings.
-R  Recursively list subdirectories encountered.
-r  Reverse the order of the sort.
-S  Sort by size, largest file first.
-s  Display file system blocks (1 block = 512 bytes) actually used by each file.
-T  Displays the second and the year along with the other time. Has no effect
    unless -g, -l, or -n are used.
-t  Sort by time modified, most recent first.
-u  Use file's last access time instead of last modification time for -t.
-x  Compatability.
", argv0);
    process::exit(1);
}

// Idk how else to clone path for mutability/lifetimes while still keeping a static size
#[allow(clippy::clone_double_ref)]
fn gen_line(path: &path::Path, args: Args, from_recursed: bool) -> Option<(String, fs::Metadata)> {
    let mut out = String::new();

    let mut path = path.clone();

    if !path.exists() {
        return None;
    }

    let metadata = path.metadata().unwrap();
    let perms = metadata.permissions();

    let sym_metadata = fs::symlink_metadata(path).unwrap();

    let canonbuf = fs::canonicalize(path).unwrap();
    if sym_metadata.file_type().is_symlink()
        && (args.fsh_follow_cmd && !from_recursed)
        || args.fsl_follow_info {
        path = path::Path::new(&canonbuf);
    }

    if args.fi_inodes {
        out.push_str(&format!("{} ", metadata.ino().to_string()));
    }

    if args.fs_display_blocks {
        if args.fk_kilos {
            // 1 kb = 1024. blocks() counts in terms of 512 byte blocks. 512 * 2 = 1024 so it should be divided.
            out.push_str(&format!("{} ", metadata.blocks() / 2));
        } else {
            out.push_str(&format!("{} ", metadata.blocks()));
        }
    }

    if args.fl_long {
        /* perms */
        // This is just a bunch of bitwise stuff. Might put it in common somewhere with my own impl
        // instead of a crate that hasn't been updated in years.
        out.push_str(&format!("{} ", unix_mode::to_string(perms.mode())));

        /* file count */
        // If it's a dir, the dir is counted as a file
        let mut fcount = 1;
        if path.is_dir() {
            if let Ok(rd) = path.read_dir() {
                fcount += rd.map(|e| e.map(|e| e.path()))
                    .collect::<Result<Vec<_>, io::Error>>()
                    .unwrap_or_default()
                    .len();
            }
        }

        out.push_str(&format!("{} ", fcount));

        /* usr/grp */
        let usrgrp = if args.fn_uidgid_numeric {
            format!("{} {} ",
                if !args.fg_no_owner {metadata.uid()} else {0},
                metadata.gid()
            )
        } else {
            let usr = users::get_user_by_uid(metadata.uid()).unwrap();
            let grp = users::get_group_by_gid(metadata.gid()).unwrap();
            format!("{} {} ",
                if !args.fg_no_owner {usr.name().to_string_lossy().to_string()} else {String::new()},
                grp.name().to_string_lossy()

            )
        };
        out.push_str(&usrgrp);
        /* file size */
        if args.fh_units {
            let size = metadata.size();
            if size > 107372544 { // Gigabytes
                out.push_str(&format!("{:.1}G ", size as f32 / 107372544.0));
            } else if size > 104856 { // Megabytes
                out.push_str(&format!("{:.1}M ", size as f32 / 104856.0));
            } else if size > 1024 { // Kilobytes
                out.push_str(&format!("{:.1}K ", size as f32 / 1024.0));
            } else { // Bytes
                out.push_str(&format!("{}B ", size));
            }
        } else {
            out.push_str(&format!("{} ", metadata.size()));
        }

        /* date/time */
        let duration_since_epoch = UNIX_EPOCH + Duration::from_secs(metadata.mtime() as u64);
        let dt = DateTime::<Local>::from(duration_since_epoch);
        if args.fst_display_full_time {
            out.push_str(&dt.format("%b %e %T %Y ").to_string());
        } else {
            out.push_str(&dt.format("%b %e %R ").to_string());
        }
    }

    let mut file = path.to_string_lossy().to_string();
    if file.starts_with("./") {
        file = file.replacen("./", "", 1);
    }

    out.push_str(&file);

    if args.fsf_exe_sym {
        if sym_metadata.file_type().is_symlink() {
            out.push('@');
        }

        if metadata.is_file() && perms.mode() & 0o111 != 0 {
            out.push('*');
        }

    }

    if args.fsp_display_path && path.is_dir() {
        out.push('/');
    }

    /* TODO: Figure out a way to show symlink path without common::io::space messing it up
    if sym_metadata.file_type().is_symlink() {
        out.push_str(&format!("->{}", canonbuf.to_string_lossy().to_string()));
    }
    */

    Some((out, metadata))
}

fn ls(path: &path::Path, args: Args, recursed: bool) {
    let mut out = Vec::new();
    let pathname = path.to_string_lossy().to_string();
    if path.is_dir() {
        match path.read_dir() {
            Ok(read_dir) => {
                let mut entries = read_dir
                    .map(|e| e.map(|e| e.path()))
                    .collect::<Result<Vec<_>, io::Error>>().unwrap_or_default();

                entries.sort();

                entries.iter()
                    .filter(|p| if p.file_name().unwrap().to_string_lossy().starts_with('.') {args.fsa_list_all} else {true})
                    .for_each(|e| {
                        if e.is_dir() && args.fsr_recurse {
                            // Recursion :flushed:
                            ls(e, args, true);
                        } else if let Some(l) = gen_line(e, args, recursed) {
                            out.push(l);
                        }
                    });
            },
            Err(e) => eprintln!("Error reading {}: {}", pathname, e),
        }
    } else if let Some(l) = gen_line(path, args, recursed) {
        out.push(l);
    }

    if args.fc_sort_last_changed || args.ft_sort_modified || args.fu_sort_access_time {
        out.sort_unstable_by(|(_al, am), (_bl, bm)| {
            let (atime, btime) = {
                if args.fc_sort_last_changed {
                    (am.ctime() as u64, bm.ctime() as u64)
                } else if args.ft_sort_modified {
                    (am.mtime() as u64, bm.mtime() as u64)
                } else {
                    (am.atime() as u64, bm.atime() as u64)
                }
            };

            let adur = UNIX_EPOCH + Duration::from_secs(atime);
            let adt = DateTime::<Local>::from(adur);
            let bdur = UNIX_EPOCH + Duration::from_secs(btime);
            let bdt = DateTime::<Local>::from(bdur);
            bdt.partial_cmp(&adt).unwrap()
        });
    }

    if args.fr_reverse {
        out = out.iter().rev().map(|(s, m)| (s.clone(), m.clone())).collect();
    }

    let mut lines = out.iter().map(|(line, _)| {
        if args.fl_long {
            let mut split = line.split_whitespace().collect::<Vec<_>>();
            let line = split.pop().unwrap();
            if let Some(stripped) = line.strip_prefix(&format!("{}/", path.to_string_lossy().to_string())) {
                format!("{} {}", split.join(" "), stripped)
            } else {
                format!("{} {}", split.join(" "), line)
            }
        } else if let Some(stripped) = line.strip_prefix(&format!("{}/", path.to_string_lossy().to_string())) {
            stripped.to_string()
        } else {
            line.to_string()
        }
    }).collect();

    if args.fl_long {
        lines = common::io::space(lines, 1);
    }

    if args.fm_stream {
        println!("{}", lines.join(", "));
    } else {
        if args.fsc_columns {
            let mut longest = 0;
            lines.iter().for_each(|e| if e.len() > longest {longest = e.len();});
            longest += 1;

            let cols = if let Some((cols, _)) = term_size::dimensions() {
                cols - longest
            } else {
                80
            };
            let mut idx = 0;
            for entry in lines {
                print!("{:space$}", entry, space=longest);
                idx += longest;
                if idx >= cols {println!(); idx = 0;}
            }
            if idx != 0 {println!();}
        } else {
            let info = unsafe {libc::isatty(libc::STDOUT_FILENO) != 0} && recursed;
            if info {println!("{}:", pathname)}
            println!("{}", lines.join("\n"));
            if info {println!()}
        }
    }
}

fn main() {
    let mut argv0 = String::new();
    let mut fsa_list_all = false;
    let mut fc_sort_last_changed = false;
    let mut fsc_columns = unsafe {libc::isatty(libc::STDOUT_FILENO) != 0};
    let mut fsf_exe_sym = false;
    let mut fg_no_owner = false;
    let mut fsh_follow_cmd = false;
    let mut fh_units = false;
    let mut fi_inodes = false;
    let mut fk_kilos = false;
    let mut fsl_follow_info = false;
    let mut fl_long = false;
    let mut fm_stream = false;
    let mut fn_uidgid_numeric = false;
    let mut fsp_display_path = false;
    let mut fsr_recurse = false;
    let mut fr_reverse = false;
    let mut fss_sort_size = false;
    let mut fs_display_blocks = false;
    let mut fst_display_full_time = false;
    let mut ft_sort_modified = false;
    let mut fu_sort_access_time = false;

    let mut dirnames = argbegin! {
        &mut argv0,
        '1' => fsc_columns = false,
        'A' => fsa_list_all = true,
        'a' => fsa_list_all = true,
        'C' => fsc_columns = true,
        'c' => {fc_sort_last_changed = true; fss_sort_size = false; ft_sort_modified = false; fu_sort_access_time = false;},
        'd' => {},
        'F' => {fsf_exe_sym = true; fsp_display_path = true;},
        'f' => {},
        'g' => {fl_long = true; fg_no_owner = true;},
        'H' => fsh_follow_cmd = true,
        'h' => fh_units = true,
        'i' => fi_inodes = true,
        'k' => fk_kilos = true,
        'L' => fsl_follow_info = true,
        'l' => {fl_long = true; fsc_columns = false; fm_stream = false;},
        'm' => {fm_stream = true; fl_long = false;},
        'n' => {fl_long = true; fn_uidgid_numeric = true;},
        'o' => {},
        'P' => fsp_display_path = true,
        'q' => {},
        'R' => fsr_recurse = true,
        'r' => fr_reverse = true,
        'S' => {fss_sort_size = true; fc_sort_last_changed = false; ft_sort_modified = false; fu_sort_access_time = false;},
        's' => fs_display_blocks = true,
        'T' => fst_display_full_time = true,
        't' => {ft_sort_modified = true; fss_sort_size = false; fc_sort_last_changed = false; fu_sort_access_time = false;},
        'u' => {fu_sort_access_time = true; ft_sort_modified = false; fss_sort_size = false; fc_sort_last_changed = false;},
        'x' => {},
        _ => help(&argv0)
    };

    let mut args = Args::new();
    args.fc_sort_last_changed(fc_sort_last_changed)
        .fsc_columns(fsc_columns)
        .ft_sort_modified(ft_sort_modified)
        .fu_sort_access_time(fu_sort_access_time)
        .fg_no_owner(fg_no_owner)
        .fh_units(fh_units)
        .fi_inodes(fi_inodes)
        .fk_kilos(fk_kilos)
        .fl_long(fl_long)
        .fm_stream(fm_stream)
        .fn_uidgid_numeric(fn_uidgid_numeric)
        .fr_reverse(fr_reverse)
        .fs_display_blocks(fs_display_blocks)
        .fsa_list_all(fsa_list_all)
        .fsf_exe_sym(fsf_exe_sym)
        .fsh_follow_cmd(fsh_follow_cmd)
        .fsl_follow_info(fsl_follow_info)
        .fsp_display_path(fsp_display_path)
        .fsr_recurse(fsr_recurse)
        .fst_display_full_time(fst_display_full_time);

    if dirnames.is_empty() {dirnames.push(".".to_string());}

    for dirname in dirnames {
        let path = path::Path::new(&dirname);
        if !path.exists() {
            eprintln!("\"{}\" doesn't exist.", dirname);
            continue;
        }
        ls(path, args, false);
    }
}
