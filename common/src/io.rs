use std::io::*;

pub fn yes_no(prompt: &str, stderr: bool) -> bool {
    if stderr {
        eprint!("{} [y/N]: ", prompt);
    } else {
        print!("{} [y/N]: ", prompt);
    }
    stdout().flush().expect("Couldn't flush stdout.");
    let mut input = String::new();
    stdin().read_line(&mut input).expect("Couldn't get user input.");

    input.trim().to_lowercase().starts_with('y')
}

/// Takes in a Vec<String> and a minimum amount of whitespace.
/// Current impl is very naive and probably has a lot of bugs and performance issues (but it works with ls -l :D)
pub fn space(input: Vec<String>, whitespace: usize) -> Vec<String> {
    let mut out = vec![String::new(); input.len()];

    let mut longest_split = 0;
    input.iter().for_each(|l| {
        let cnt = l.split_whitespace().count();
        if cnt > longest_split {longest_split = cnt}
    });

    let mut split_idx = 0;

    let splits: Vec<Vec<&str>> = input
        .iter()
        .map(|l| l.split_whitespace().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    while split_idx < longest_split {
        let mut current = splits
            .iter()
            .map(|s| s.get(split_idx).unwrap_or(&""))
            .map(|s| s.to_string())
            .collect::<Vec<String>>();

        let mut longest = 0;
        current.iter().for_each(|l| {
            let len = l.len();
            if longest < len {longest = len;}
        });
        longest += whitespace;

        current = current.iter_mut().map(|l| {
            for _ in 0..longest-l.len() {l.push(' ');}
            l
        }).map(|l| l.clone()).collect();

        for (idx, element) in out.iter_mut().enumerate() {
            element.push_str(&current[idx]);
        }
        split_idx+=1;
    }

    out
}
