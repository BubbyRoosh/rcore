use std::{fs, io};
use std::path::Path;

pub fn read_to_string_lossy<P: AsRef<Path>>(path: P) -> io::Result<String> {
    Ok(String::from_utf8_lossy(&fs::read(path)?).to_string())
}

pub fn can_alter(path: &Path, allow_dirs: bool, recurse_dirs: bool) -> bool {
    let name = path.to_string_lossy();
    if !path.exists() {
        eprintln!("\"{}\" doesn't exist.", name);
        false
    } else if path.is_dir() {
        if !allow_dirs {
            eprintln!("\"{}\" is a directory.", name);
            false
        } else if path.read_dir().unwrap().next().is_some() && !recurse_dirs {
            eprintln!("\"{}\" is a directory and is not empty.", name);
            false
        } else {
            true
        }
    } else {
        true
    }
}

pub fn should_alter(prompt: &str, dest: &Path, dont_prompt: bool, prompt_all: bool) -> bool {
    let mut should_alter = true;
    let mut prompted = false;
    if dest.exists() && !dest.is_dir() && !dont_prompt {
        should_alter = super::io::yes_no(prompt, true);
        prompted = true;
    } 
    if prompt_all && !prompted {
        should_alter = super::io::yes_no(prompt, true)
    }
    should_alter
}
