DESTDIR=${HOME}
PREFIX=/.local

BINPROGS=cat cp echo hostname ls mkdir mv pwd rm sleep
USRBINPROGS=dirname head false true tee uname wc which yes

all: install-bin install-usr.bin
	@echo Installed programs to ${DESTDIR}${PREFIX}

build-bin:
	cd bin && cargo build --release
	
install-bin: build-bin
	mkdir -p ${DESTDIR}${PREFIX}/bin
	@for prog in ${BINPROGS}; do \
		cp bin/target/release/$${prog} ${DESTDIR}${PREFIX}/bin/$${prog}; \
	done
	
build-usr.bin:
	cd usr.bin && cargo build --release

install-usr.bin: build-usr.bin
	mkdir -p ${DESTDIR}${PREFIX}/usr/bin
	for prog in ${USRBINPROGS}; do \
		cp usr.bin/target/release/$${prog} ${DESTDIR}${PREFIX}/usr/bin/$${prog}; \
	done
