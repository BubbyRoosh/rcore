use std::io::{self, BufRead};
use std::{env, process};

use common::fs;

fn usage(argv0: &str) {
    eprintln!(
"{} [-count | -n count] [file ...]

Display first few lines of files.

Copyright (c) 2021 BubbyRoosh

-<n> | -n <n>   Copy the first <n> lines of each input file to stdout.
                Must be a positive int.
", argv0);
    process::exit(1);
}

fn main() {
    let mut args: Vec<String> = env::args().collect();
    let argv0 = args.remove(0);
    let mut count: usize = 0;
    let mut filenames = Vec::new();

    let mut set_count = |maybe: &str| {
        if let Ok(c) = maybe.parse::<usize>() {
            count = c;
        } else {
            usage(&argv0);
        }
    };

    let mut skip = false;
    for (idx, arg) in args.iter().enumerate() {
        if skip {skip = false; continue;}
        if let Some(stripped) = arg.strip_prefix('-') {
            let string = if stripped.starts_with('n') {
                if stripped.len() > 1 {
                    stripped.strip_prefix('n').unwrap()
                } else if idx < args.len() {
                    skip = true;
                    &args[idx+1]
                } else {
                    "invalid"
                }
            } else {
                stripped
            };
            set_count(string);
        } else {
            filenames.push(arg);
        }
    }

    if filenames.is_empty() {
        let stdin = io::stdin();
        for (idx, line) in stdin.lock().lines().enumerate() {
            if idx >= count {break;}
            println!("{}", line.unwrap_or_default());
        }
    } else {
        for name in filenames {
            match fs::read_to_string_lossy(name) {
                Ok(contents) => {
                    for (idx, line) in contents.lines().enumerate() {
                        if idx >= count {break;}
                        println!("{}", line);
                    }
                },
                Err(e) => eprintln!("Error reading {}: {}", name, e)
            }
        }
    }
}
