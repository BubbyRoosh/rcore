use std::{env, fs, process};

use arg_dot_h::*;

fn help(argv0: &str) {
    eprintln!(
"{} [-a] name ...

Locate a program file (or files) in PATH.

Copyright (c) 2021 BubbyRoosh

Arguments:
-a  Return all matches instead of just the first match.
", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut fa_all = false;

    let mut matchcount = 0;

    let prognames = argbegin! {
        &mut argv0,
        'a' => fa_all = true,
        _ => help(&argv0)
    };

    // Might be refactored into something in common later.
    if let Ok(path) = env::var("PATH") {
        let paths = path.split(':').collect::<Vec<_>>();

        let entries = {
            let mut entries = Vec::new();
            for path in paths {
                if let Ok(rd) = fs::read_dir(path) {
                    for entry in rd.flatten() {
                        entries.push(entry.path());
                    }
                }
            }
            entries
        };

        for name in &prognames {
            let matches = entries.iter()
                .filter(|e| e.to_string_lossy().to_string().split('/').last() == Some(name)).collect::<Vec<_>>();

            if !matches.is_empty() {
                matchcount+=1;
            }

            if fa_all {
                for m in matches {
                    println!("{}", m.to_string_lossy());
                }
            } else if matchcount > 0 {
                println!("{}", matches.get(0).unwrap().to_string_lossy());
            }
        }

    } else {
        eprintln!("No PATH variable found.");
        process::exit(2);
    }

    if matchcount == prognames.len() { // All matches
        process::exit(0);
    } else if matchcount > 0 { // Some matches
        process::exit(1);
    } else { // No matches
        process::exit(2);
    }
}
