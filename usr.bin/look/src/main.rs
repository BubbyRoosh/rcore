use std::process;

use arg_dot_h::*;

use common::fs;

fn help(argv0: &str) {
    eprintln!(
"{} [-df] [-t termchar] string [file]

Display lines beginning with a given string.

Copyright (c) 2021 BubbyRoosh

If [file] is not specified, /usr/share/dict/words is used.

Arguments:
-d  Only alphanumeric characters are compared.
-f  Ignore the case of alphabetic characters.
-t  Only search up until [termchar] in <string>.", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut fd_alphanumeric = false;
    let mut ff_ignore_case = false;
    let mut ot_termchar = '\n';
    let mut extra = argbegin! {
        &mut argv0,
        'd' => fd_alphanumeric = true,
        'f' => ff_ignore_case = true, 
        't' => ot_termchar = eargf!(help(&argv0)).chars().next().unwrap(),
        _ => help(&argv0)
    };

    let mut search = if extra.is_empty() {
        help(&argv0);
        return; // Unreachable
    } else {
        extra.remove(0)
    };

    let file = if extra.len() == 1 {
        &extra[0]
    } else {
        ff_ignore_case = true;
        fd_alphanumeric = true;
        "/usr/share/dict/words"
    };

    if ff_ignore_case {
        search = search.to_lowercase();
    }

    if fd_alphanumeric {
        search = search.chars().filter(|ch| ch.is_alphanumeric()).collect();
    }

    let mut matches = 0;
    match fs::read_to_string_lossy(file) {
        Ok(contents) => {
            for line in contents.lines() {
                let mut searched = String::new();
                for ch in line.chars() {
                    if ch == ot_termchar {break;}
                    if ff_ignore_case {
                        for l in ch.to_lowercase() {
                            searched.push(l);
                        }
                    } else {
                        searched.push(ch);
                    }
                    if searched == search {
                        println!("{}", line);
                        matches += 1;
                        break;
                    }
                }
            }
        },
        Err(e) => {
            eprintln!("Error reading {}: {}", extra[0], e);
            process::exit(2);
        },
    }

    // Should return 1 if no matches are found.
    if matches == 0 {
        process::exit(1);
    }
}
