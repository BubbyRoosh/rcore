use std::{fs, process};
use std::io::{self, BufRead, Write};

use arg_dot_h::*;

use nix::sys::signal;

fn help(argv0: &str) {
    eprintln!(
"{} [-ai] [file ...]

Pipe fitting.

Copyright (c) 2021 BubbyRoosh

Arguments:
-a  Append the output to the files rather than overwriting them.
-i  Ignore the SIGINT signal.", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut fa_append = false;
    let mut fi_ignore = false;
    let filenames = argbegin! {
        &mut argv0,
        'a' => fa_append = true,
        'i' => fi_ignore = true,
        _ => help(&argv0)
    };

    if fi_ignore {
        unsafe {
            signal::signal(signal::Signal::SIGINT, signal::SigHandler::SigIgn)
        }.unwrap();
    }

    let files = filenames.iter().map(|name| {
        fs::OpenOptions::new()
            .create(true)
            .write(true)
            .append(fa_append)
            .open(name)
        }).filter_map(|result| {
            if let Err(e) = result {
                eprintln!("Error opening file: {}", e);
                None
            } else {
                Some(result.unwrap())
            }
        }).collect::<Vec<_>>();

    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.unwrap_or_default();
        files.iter().for_each(|mut file| {
            if let Err(e) = writeln!(file, "{}", line) {
                eprintln!("Couldn't write to file: {}", e);
            }
        });
        println!("{}", line);
    }
}
