use std::{env, fs, io};
use std::io::Write;
use std::os::unix::io::FromRawFd;

fn main() {
    let mut msg = match env::args().nth(1) {
        Some(msg) => msg,
        None => "y".to_string(),
    };
    
    msg.push('\n');
    let msg = msg.as_bytes();
    let stdout = unsafe { fs::File::from_raw_fd(1) };

    let mut writer = io::BufWriter::new(stdout);

    loop {
        writer.write_all(msg).unwrap();
    }
}
