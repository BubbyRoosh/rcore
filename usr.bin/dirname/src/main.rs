use std::{env, path, process}; 

fn help(argv0: &str) {
    println!("usage: {} path", argv0);
    process::exit(1);
}

fn main() {
    let mut args = env::args();
    
    let argv0 = args.next().unwrap();
    
    match args.next() {
        Some(dir) => {
            let mut cur = path::PathBuf::from(dir);
            cur.pop();
            let dirname = if cur.to_string_lossy().is_empty() {
                ".".to_string()
            } else {
                cur.to_string_lossy().to_string()
            };
            println!("{}", dirname);
        },
        None => help(&argv0),
    }
}
