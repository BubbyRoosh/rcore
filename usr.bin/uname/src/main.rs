use std::{env, process};

use arg_dot_h::*;

use nix::sys::utsname;

fn help(argv0: &str) {
    eprintln!(
"{} [-amnprsv]

Print operating system name.

Copyright (c) 2021 BubbyRoosh

Arguments:
-a  Behave as though -mnrsv were passed.
-m  Print the machine hardware name.
-n  Print the nodename.
-p  Print the machine processor architecture name.
-r  Print the operating system release.
-s  Print the operating system name (default).
-v  Print the operating system version.
", argv0);
    process::exit(1);
}

static SYSNAME:   u16 = 0x01; // 00000001
static NODENAME:  u16 = 0x02; // 00000010
static RELEASE:   u16 = 0x04; // 00000100
static VERSION:   u16 = 0x08; // 00001000
static MACHINE:   u16 = 0x10; // 00010000
static ALL:       u16 = 0x1F; // 00011111
static PARCH:     u16 = 0x20; // 00100000

fn main() {
    let mut argv0 = String::new();
    let mut mask: u16 = 0;

    argbegin! {
        &mut argv0,
        'a' => mask |= ALL,
        'm' => mask |= MACHINE,
        'n' => mask |= NODENAME,
        'p' => mask |= PARCH,
        'r' => mask |= RELEASE,
        's' => mask |= SYSNAME,
        'v' => mask |= VERSION,
        _ => help(&argv0)
    };

    if mask == 0 {
        mask = SYSNAME;
    }

    let uname = utsname::uname();

    if mask & SYSNAME != 0 {
        print!("{} ", uname.sysname());
    }
    if mask & NODENAME != 0 {
        print!("{} ", uname.nodename());
    }
    if mask & RELEASE != 0 {
        print!("{} ", uname.release());
    }
    if mask & VERSION != 0 {
        print!("{} ", uname.version());
    }
    if mask & MACHINE != 0 {
        print!("{} ", uname.machine());
    }
    if mask & PARCH != 0 {
        print!("{} ", env::consts::ARCH);
    }
    println!();
}
