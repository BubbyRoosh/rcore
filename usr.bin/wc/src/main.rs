use std::{fs, process};
use std::io::{self, BufRead};

use arg_dot_h::*;

fn help(argv0: &str) {
    eprintln!(
"{} [-c | -m] [-lw] [file ...]

Word, line, and byte character count.

Copyright (c) 2021 BubbyRoosh

Arguments:
-c  The number of bytes in each input file is written to the standard output.
-l  The number of lines in each input file is written to the standard output.
-m  Count characters instead of bytes.
-w  The number of words in each input file is written to the standard output.
", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut fc_bytes = false;
    let mut fl_lines = false;
    let mut fm_chars = false;
    let mut fw_words = false;

    let files = argbegin! {
        &mut argv0,
        'c' => fc_bytes = true,
        'l' => fl_lines = true,
        'm' => fm_chars = true,
        'w' => fw_words = true,
        _ => help(&argv0)
    };

    if !fc_bytes && !fl_lines && !fm_chars && !fw_words {
        fc_bytes = true;
        fl_lines = true;
        fm_chars = true;
        fw_words = true;
    }

    let wc = |lines: Vec<String>, name: &str, bytes: Vec<u8>| -> String {
        let mut output = String::new();

        if fl_lines {
            output.push_str(&lines.len().to_string());
        }

        if fw_words {
            output.push_str(&format!(" {}", lines.iter().fold(0, |acc, l| acc + l.split_whitespace().count())));
        }

        if fc_bytes {
            output.push_str(&format!(" {}", bytes.len()));
        } else if fm_chars {
            output.push_str(&format!(" {}", lines.iter().fold(0, |acc, l| acc + l.chars().count()) + lines.len()));
        }
        output.push_str(&format!(" {}", name));
        output
    };

    if files.is_empty() {
        let stdin = io::stdin();
        let lines = stdin.lock().lines().map(|l| l.unwrap_or_default()).collect::<Vec<_>>();
        let joined = lines.join("\n");
        println!("{}", wc(lines, "", joined.bytes().collect::<Vec<_>>()));
    } else {
        files.iter().for_each(|f| {
            match fs::read(f) {
                Ok(b) => {
                    let s = String::from_utf8_lossy(&b);
                    println!("{}", wc(s.lines().map(|l| l.to_owned()).collect::<Vec<_>>(), f, b));
                }
                Err(e) => println!("Error reading {}: {}", f, e),
            }
        });
    }
}
