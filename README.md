# rcore

Some core OpenBSD utilities somewhat rewritten in rust :D


## Currently implemented

Some of these programs may be buggy. They haven not been fully tested.

### /bin

* cat
* cp
* echo
* hostname
* ls
* mkdir
* mv
* pwd
* rm
* sleep

### /usr/bin

* dirname
* false
* head
* look
* true
* tee
* uname
* wc
* which
* yes

## TODO

### /bin

* [ (alias to test?)
* chgrp
* chmod
* date
* dd
* expr
* kill
* ln
* ps
* rmdir
* sh
* tar
* test

### /usr/bin

* arch/machine
* clear
* cmp
* cut
* doas
* du
* env
* find
* finger
* fold
* getopt
* id
* less
* lock
* pkill
* printf
* split
* tail
* time
* tr
* uptime
* whoami
